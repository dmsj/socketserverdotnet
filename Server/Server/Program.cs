﻿using Server.src.Configuration;
using Server.src.Listeners;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Config.Load();
            SocketListener.StartListening();
        }
    }
}
