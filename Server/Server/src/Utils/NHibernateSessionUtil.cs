﻿using NHibernate;
using NHibernate.Cfg;
using System;

namespace LeagueEmulator.Domain.Utils
{
    public class NHibernateSessionUtil
    {
        private static ISessionFactory _sessionFactory;

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    try
                    {
                        var configuration = new Configuration();
                        configuration.Configure();
                        _sessionFactory = configuration.BuildSessionFactory();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                }
                return _sessionFactory;
            }
        }

        public static ISession OpenSession()
        {
            if (SessionFactory == null)
            {
                return null;
            }
            return SessionFactory.OpenSession();
        }
    }
}
