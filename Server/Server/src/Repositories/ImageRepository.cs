﻿using LeagueEmulator.Domain.Utils;
using NHibernate;
using Domain.Model;
using System.Collections.Generic;
using System;

namespace Server.src.Repositories
{
    public class ImageRepository
    {
        private ISession _session;

        private ISession currentSession()
        {
            if (_session == null)
            {
                _session = NHibernateSessionUtil.OpenSession();
            }
            return _session;
        }
        public IEnumerable<ClientImage> ClientImageList
        {
            get
            {
                IEnumerable<ClientImage> clientImageList = null;
                try
                {
                    clientImageList = currentSession().QueryOver<ClientImage>().List();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                return clientImageList;
            }
        }
        public bool Save(ClientImage img)
        {
            bool result = true;
            using (ITransaction transaction = currentSession().BeginTransaction())
            {
                try
                {
                    currentSession().SaveOrUpdate(img);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    result = false;
                    transaction.Rollback();
                    Console.WriteLine(e.StackTrace);
                }
            }
            return result;
        }
    }
}
