﻿using Domain.Model;
using Server.src.Configuration;
using Server.src.Handlers;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Drawing;

namespace Server.src.Listeners
{
    public class SocketListener
    {
        private static CommandHandlerContainer commandContainer;
        public static ServerResponse Response { get; set; }
        public static CommandHandlerContainer CommandContainer
        {
            get
            {
                if (commandContainer == null)
                {
                    commandContainer = new CommandHandlerContainer();
                }
                return commandContainer;
            }
        }
        public SocketListener()
        {
        }

        public static void StartListening()
        {
            TcpListener server = null;
            TcpClient client = null;
            try
            {
                IPAddress localAddr = IPAddress.Parse(Config.HOST_IP);

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, Config.PORT);

                // Start listening for client requests.
                server.Start();

                // Enter the listening loop.
                while (true)
                {
                    Console.WriteLine("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();
                    try
                    {
                        IFormatter formatter = new BinaryFormatter();

                        Command cmd = (Command)formatter.Deserialize(stream); // you have to cast the deserialized object 

                        Console.WriteLine("Received command " + cmd.ClientCommand.ToString());

                        //data = Encoding.ASCII.GetString(bytes, 0, i);
                        CommandContainer.HandleCommand(cmd);
                        formatter.Serialize(stream, Response); // the serialization process 
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine("Connection with host lost...");
                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }


            Console.WriteLine("\nHit enter to continue...");
            Console.Read();
        }
    }
}
