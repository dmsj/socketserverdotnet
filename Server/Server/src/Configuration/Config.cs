﻿using System;
using System.Configuration;

namespace Server.src.Configuration
{
    public class Config
    {
        public static int PORT = 11000;
        public static string HOST_IP = "127.0.0.1";

        public static void Load()
        {
            try
            {
                PORT = int.Parse(ConfigurationManager.AppSettings["port"]);
                HOST_IP = ConfigurationManager.AppSettings["ip"];
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}
