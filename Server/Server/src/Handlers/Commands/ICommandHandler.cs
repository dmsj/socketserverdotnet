﻿
using Domain.Model;

namespace Server.src.Handlers.Commands
{
    public interface ICommandHandler
    {
        bool HandleCommand(Command command);
    }
}
