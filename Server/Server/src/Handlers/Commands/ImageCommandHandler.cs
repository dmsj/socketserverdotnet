﻿using Domain.Enums;
using Domain.Model;
using Server.src.Listeners;
using Server.src.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Server.src.Handlers.Commands
{
    class ImageCommandHandler : ICommandHandler
    {
        private ImageRepository repo;
        public ImageRepository Repository
        {
            get
            {
                if (repo == null)
                {
                    repo = new ImageRepository();
                }
                return repo;
            }
        }
        private ClientCommand[] availableCommands = { ClientCommand.GET_IMAGES, ClientCommand.SAVE_IMAGE };
        public bool HandleCommand(Command command)
        {
            Console.WriteLine("Command to handle " + command.ClientCommand.ToString());
            if (availableCommands.Contains(command.ClientCommand))
            {
                switch (command.ClientCommand)
                {
                    case ClientCommand.GET_IMAGES:
                        SocketListener.Response = new ServerResponse() { ClientCommand = command.ClientCommand, ClientImageList = Repository.ClientImageList };
                        break;
                    case ClientCommand.SAVE_IMAGE:
                        if (!(command.Data is ClientImage))
                        {
                            return false;
                        }
                        ClientImage clientImg = new ClientImage();
                        //Image img = byteArrayToImage(((ClientImage)command.Data).Img);
                        Console.WriteLine("Image name " + ((ClientImage)command.Data).Img);
                        clientImg.Name = ((ClientImage)command.Data).Name;
                        clientImg.Img = ((ClientImage)command.Data).Img;
                        Repository.Save(clientImg);
                        SocketListener.Response = new ServerResponse() { ClientCommand = command.ClientCommand };
                        break;
                }
                return true;
            }
            return false;
        }
        private byte[] imageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }
        private Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
