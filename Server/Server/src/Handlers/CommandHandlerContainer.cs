﻿using Domain.Model;
using Server.src.Handlers.Commands;
using Server.src.Listeners;
using System;
using System.Collections.Generic;

namespace Server.src.Handlers
{
    public class CommandHandlerContainer
    {
        public CommandHandlerContainer()
        {
            registerCommand(new ImageCommandHandler());
        }
        private IList<ICommandHandler> commands;
        public IList<ICommandHandler> Commands
        {
            get
            {
                if (commands == null)
                {
                    commands = new List<ICommandHandler>();
                }
                return commands;
            }
        }
        private void registerCommand(ICommandHandler command)
        {
            Commands.Add(command);
        }
        public void HandleCommand(Command command)
        {
            foreach (ICommandHandler handler in Commands)
            {
                if (handler.HandleCommand(command))
                {
                    Console.WriteLine("Command handled by " + handler.GetType().Name);
                    return;
                }
            }
            //AsynchronousSocketListener.Response = ClientCommand.NONE.ToString();
        }
    }
}
